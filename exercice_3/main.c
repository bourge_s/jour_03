#include <string.h>
#include "bc.h"
#include "Garde.h"
#include <stdlib.h>

void	garde_de_nuit(Corbeau *, char *, int, Maison);


int	main(void)
{
  Corbeau test;

  bc_write_str("=======================================\n");
  bc_write_str("Création de : Jon Snow, 18 ans, Stark.\n");
  bc_write_str("=======================================\n");
  bc_write_str("Résultat : ");
  garde_de_nuit(&test, "Jon Snow", 18, STARK);
  if (test._age == 18 && test._maison == STARK && strcmp("Jon Snow", test._nom) == 0)
    bc_write_str("Ok.\n");
  else
    bc_write_str("Erreur.\n");
  bc_write_str("\n=======================================\n");
  bc_write_str("Création de : Daenerys, 30 ans, Targaryen.\n");
  bc_write_str("=======================================\n");
  bc_write_str("Résultat : ");
  garde_de_nuit(&test, "Daenerys", 30, TARGARYEN);
  if (test._age == 30 && test._maison == TARGARYEN && strcmp("Daenerys", test._nom) == 0)
    bc_write_str("Ok.\n");
  else
    bc_write_str("Erreur.\n");
  bc_write_str("\n=======================================\n");
  bc_write_str("Création de : Stannis, 50 ans, Baratheon.\n");
  bc_write_str("=======================================\n");
  bc_write_str("Résultat : ");
  garde_de_nuit(&test, "Stannis", 50, BARATHEON);
  if (test._age == 50 && test._maison == BARATHEON && strcmp("Stannis", test._nom) == 0)
    bc_write_str("Ok.\n");
  else
    bc_write_str("Erreur.\n");
  bc_write_str("\n=======================================\n");
  bc_write_str("Création de : Cersei, 45 ans, Lannister.\n");
  bc_write_str("=======================================\n");
  bc_write_str("Résultat : ");
  garde_de_nuit(&test, "Cersei", 45, LANNISTER);
  if (test._age == 45 && test._maison == LANNISTER  && strcmp("Cersei", test._nom) == 0)
    bc_write_str("Ok.\n");
  else
    bc_write_str("Erreur.\n");
}
